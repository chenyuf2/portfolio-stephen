import "./App.scss";
import Content from "./components/Content/Content";
import Preloader from "./components/Preloader/Preloader";

import React, { useState } from "react";

const App = () => {
  const [loading, setLoading] = useState(true);
  return (
    <React.Fragment>
      <Preloader loading={loading} setLoading={setLoading} />
      <Content loading={loading} />
    </React.Fragment>
  );
};

export default App;
