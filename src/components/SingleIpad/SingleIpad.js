import styles from "./SingleIpad.module.scss";
import React from "react";
import { Reveal, Tween } from "react-gsap";
import { fadeInDuration } from "../../utils/utilVar";
import { useEffect } from "react";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import HeaderTitle from "../HeaderTitle/HeaderTitle";

const COLORTHEORY = "Color Theory";
const SingleIpad = ({ title, isVideo, description, imageUrl, smallTitle }) => {
  gsap.registerPlugin(ScrollTrigger);
  useEffect(() => {
    gsap.fromTo(
      ".gradient-title-target",
      {
        backgroundImage: `linear-gradient(
          45deg,
          #fc4a1a 0%,
          #f7b733 100%
      )`,
      },
      {
        backgroundImage: "linear-gradient(45deg, #00f260 0%, #0575e6 65%)",
        scrollTrigger: {
          trigger: ".gradient-title-trigger",
          start: "top 55%",
          end: "top 5%",
          scrub: 0.5,
        },
      }
    );
  }, []);

  useEffect(() => {
    gsap.fromTo(
      ".gradient-content-target",
      {
        backgroundImage:
          "linear-gradient(45deg, #101010 -200%, #eee -100%, #101010 0%)",
      },
      {
        backgroundImage:
          "linear-gradient(45deg, #989898 -200%, #989898 -100%, #989898 0%)",
        scrollTrigger: {
          trigger: ".gradient-title-target",
          start: "top 80%",
          end: "top 45%",
          scrub: true,
        },
      }
    );
  }, []);

  return (
    <section
      className={[
        "overflow-hidden w-100 d-flex flex-column align-items-center position-relative bg-main z-index-2 section-padding",
        title === COLORTHEORY && "gradient-title-trigger",
      ].join(" ")}
    >
      <div className="container d-flex flex-column justify-content-center">
        {title !== COLORTHEORY ? (
          isVideo === "false" ? (
            <HeaderTitle title={title} description={description} />
          ) : (
            <div className="col-md-8 block-center">
              <h2 className="title-headline">{title}</h2>
              <div className="content-headline subtext-color">
                I am more than happy to know more people who have same interests
                as mine. Send me an email by{" "}
                <span
                  className="text-white font-weight-bold"
                  style={{
                    textDecoration: "underline",
                    textDecorationColor: "#FC466B",
                  }}
                >
                  fcy82539870@gmail.com
                </span>{" "}
                to chat.
              </div>
            </div>
          )
        ) : (
          <div className="col-md-8 block-center">
            <h2
              className={[
                "title-headline gradient-title-target",
                styles["gradient-text"],
              ].join(" ")}
            >
              {title}
            </h2>
            <div
              className={[
                "content-headline gradient-content-target",
                styles["gradient-text"],
              ].join(" ")}
            >
              {description}
            </div>
          </div>
        )}
      </div>
      <div className={styles["single-ipad-container"]}>
        <div
          className={[
            "c-single-ipad d-flex flex-column justify-content-center align-items-center",
          ].join(" ")}
        >
          <div className="ipad">
            {isVideo === "false" ? (
              <img src={imageUrl} alt="inputImg"></img>
            ) : (
              <video
                autoPlay={true}
                loop={true}
                src={imageUrl}
                alt="inputVideo"
                muted
                playsInline
              ></video>
            )}
          </div>
          {smallTitle && (
            <div
              className={[
                "w-100 text-right subtext-color font-weight-bold small-title-font mt-3",
                styles["small-title-container"],
              ].join(" ")}
            >
              {smallTitle}
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default SingleIpad;
