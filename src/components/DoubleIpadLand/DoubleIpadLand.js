import React from "react";
import { Tween } from "react-gsap";
import { fadeInDuration } from "../../utils/utilVar";
import HeaderTitle from "../HeaderTitle/HeaderTitle";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { gsap } from "gsap";
const DoubleIpadLand = ({
  title,
  imageUrlLeft,
  imageUrlRight,
  description,
}) => {
  gsap.registerPlugin(ScrollTrigger);
  return (
    <section className="section-padding d-flex justify-content-center flex-column align-items-center overflow-hidden position-relative bg-main z-index-2">
      <div className="container">
        <HeaderTitle title={title} description={description} />
      </div>

      <div className="col-md-8 c-double-ipad d-flex justify-content-center landscape-left">
        <div className="double-ipad-column">
          <Tween
            from={{
              yPercent: 10,
              xPercent: -6,
            }}
            to={{
              yPercent: 0,
              xPercent: 0,
              scrollTrigger: {
                trigger: ".doubleIpadLandScrollAnimationLeft",
                start: "top bottom",
                end: "top 10%",
                scrub: 0.5,
              },
            }}
          >
            <div className="ipad">
              <img
                src={imageUrlLeft}
                alt="abstractLand"
                className="doubleIpadLandScrollAnimationLeft"
              ></img>
            </div>
          </Tween>
        </div>
        <div className="double-ipad-column">
          <div>
            <Tween
              from={{
                yPercent: 15,
                xPercent: 8,
              }}
              to={{
                yPercent: -5,
                xPercent: 0,
                scrollTrigger: {
                  trigger: ".doubleIpadLandScrollAnimationRight",
                  start: "top bottom",
                  end: "top -20%",
                  scrub: 0.5,
                },
              }}
            >
              <div className="ipad">
                <img
                  src={imageUrlRight}
                  alt="abstractLand"
                  className="doubleIpadLandScrollAnimationRight"
                ></img>
              </div>
            </Tween>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DoubleIpadLand;
