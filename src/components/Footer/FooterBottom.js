const FooterBottom = () => {
  return (
    <footer className="container p-2">
      <hr style={{ borderTop: "1px solid #1a1a1a" }}></hr>
      <div className="row">
        <div className="col-md-12 text-center">
          <small>
            <p className="subsubtext-color font-small bold-1">
              Copyright 2021 &#169; Stephen Fan. All rights reserved. Designed
              and coded by Stephen Fan.
            </p>
          </small>
        </div>
      </div>
    </footer>
  );
};

export default FooterBottom;
