const Tools = () => {
  return (
    <section
      className="pt-5 skills-container pb-5 position-relative"
      style={{ zIndex: 2, backgroundColor: "#101010" }}
    >
      <div className="container">
        <hr style={{ borderTop: "1px solid #989898" }}></hr>
        <div className="row align-items-center m-0">
          <div className="col-md-12 p-0">
            <h5 className="subsubtext-color bold-5">Web Development</h5>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <h1 className="bold-5">Frameworks</h1>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <div className="d-flex">
              <h3 className="subtext-color bold-7">React</h3>
              <h3 className="ml-4 subtext-color bold-7">Vue</h3>
              <h3 className="ml-4 subtext-color bold-7">Angular</h3>
            </div>
          </div>
        </div>
        <div className="row align-items-center m-0 mt-5">
          <div className="col-md-12 p-0">
            <h5 className="subsubtext-color bold-5 mt-2">Programming</h5>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <h1 className="bold-5">Tools</h1>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <div className="d-flex">
              <h3 className="subtext-color bold-7">HTML</h3>
              <h3 className="ml-4 subtext-color bold-7">CSS</h3>
              <h3 className="ml-4 subtext-color bold-7">JavaScript</h3>
            </div>
          </div>
        </div>
        <div className="row align-items-center m-0 mt-5">
          <div className="col-md-12 p-0">
            <h5 className="subsubtext-color bold-5">Visual</h5>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <h1 className="bold-5">Design</h1>
          </div>
          <div className="col-md-6 p-0 mt-2">
            <div className="d-flex">
              <h3 className="subtext-color bold-7">Figma</h3>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Tools;

//test
