import { Controller, Scene } from "react-scrollmagic";
import { Tween, Timeline, Reveal } from "react-gsap";
import landscape2 from "../../images/landscape3.jpg";
import landSearch from "../../images/land-search.png";
import styles from "./ScrollAnimation.module.scss";
import React, { useEffect, useState, useRef } from "react";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import HeaderTitle from "../HeaderTitle/HeaderTitle";
import appleIcon from "../../images/apple-developer-icon.png";
import figmaIcon from "../../images/figma-icon.png";
import reactIcon from "../../images/react-icon.png";
import visualStudioIcon from "../../images/visual-studio-icon.png";
import bootstrapIcon from "../../images/bootstrap-icon.png";

const DeveloperTool = ({
  developerImg,
  tooleName,
  isImage = false,
  last = false,
}) => {
  return (
    <div className={styles["tools-sticky"]}>
      <Timeline
        target={
          <React.Fragment>
            <div
              className={[
                "text-nowrap SF-Pro-Dispay-Semibold tool-title display-flex align-items-center justify-content-center text-white z-index-2 position-relative",
              ].join(" ")}
            >
              {tooleName}
            </div>
            <div className="display-flex justify-content-center position-relative z-index-1">
              {isImage && (
                <img
                  src={developerImg}
                  className={styles["developer-img"]}
                ></img>
              )}
            </div>
          </React.Fragment>
        }
      >
        <Tween
          from={{ opacity: 0, scale: 6 }}
          to={{ opacity: 1, display: "block", scale: 1 }}
          target={1}
          duration={15}
        ></Tween>
        <Tween
          from={{ opacity: 0, scale: 8 }}
          to={{ opacity: 1, display: "block", scale: 1 }}
          target={0}
          position="-=5"
          duration={15}
        ></Tween>
        {!last && (
          <Tween to={{ opacity: 0, display: "none" }} target={0}></Tween>
        )}
        {!last && (
          <Tween
            to={{ opacity: 0, display: "none" }}
            target={1}
            position="-=1"
          ></Tween>
        )}
      </Timeline>
    </div>
  );
};

const ScrollAnimation = () => {
  gsap.registerPlugin(ScrollTrigger);
  return (
    <section
      className={[
        "desktop-view pb-5 animation-block-center section-padding",
      ].join(" ")}
    >
      <div className="container">
        <HeaderTitle
          title="Animation"
          description="Animation is complicated. I'm constantly looking at ways to optimise
            user experiences with different animations on text and images. There
            are no best ways to use animations, but only beautiful ways."
        />
      </div>
      <Controller>
        <Scene triggerHook="onLeave" duration={4000} pin>
          {(progress) => (
            <div
              className={[
                styles["sticky"],
                "d-flex justify-content-center align-items-center flex-column",
              ].join(" ")}
            >
              <Timeline totalProgress={progress} paused>
                <div
                  className="d-flex justify-content-center align-items-center expand-animation-container flex-column"
                  style={{
                    minHeight: "100vh",
                    width: "100vw",
                  }}
                >
                  <Tween
                    from={{ transform: `scale(2.2)` }}
                    to={{ transform: "scale(1)" }}
                    duration={10}
                  >
                    <div className="c-single-ipad">
                      <div className="ipad">
                        <div
                          className="position-absolute d-flex align-items-center justify-content-center"
                          style={{
                            top: "0",
                            left: "0",
                            right: "0",
                            bottom: "0",
                            zIndex: "10",
                          }}
                        >
                          <Timeline
                            target={
                              <div className="col-md-8">
                                <h2 className="font-weight-bold text-center sf-pro-family line-height-1">
                                  "People ignore design
                                </h2>
                                <h2 className="font-weight-bold text-center sf-pro-family line-height-1">
                                  that ignores people."
                                </h2>
                                <h6 className="text-center small font-weight-bold sf-pro-family mt-2">
                                  Frank Chimero
                                </h6>
                              </div>
                            }
                          >
                            <Tween
                              from={{ opacity: 0 }}
                              to={{ opacity: 1 }}
                              duration={8}
                            />
                            <Tween to={{ opacity: 0 }} duration={15} />
                          </Timeline>
                        </div>
                        <div>
                          <img src={landscape2} alt="abstractLand"></img>
                          <Tween
                            from={{ opacity: 0 }}
                            to={{ opacity: 1, delay: 1 }}
                            duration={12}
                          >
                            <img
                              src={landSearch}
                              alt="abstractLand"
                              className="position-absolute"
                              style={{
                                zIndex: "2",
                                top: "0",
                                left: "0",
                                right: "0",
                              }}
                            ></img>
                          </Tween>
                        </div>
                      </div>
                    </div>
                  </Tween>
                </div>
                {/* additional space for ipad shadow */}
                <div className="w-100" style={{ height: "6rem" }}></div>
              </Timeline>
            </div>
          )}
        </Scene>
      </Controller>
    </section>
  );
};

export default ScrollAnimation;
