import abstractLand from "../../images/abstract-land.png";
import landClothes from "../../images/land-clothes.png";
import vertClothes from "../../images/vert-clothes.png";
import landSearch from "../../images/land-search.png";
import landDark3 from "../../images/land-dark3.png";
import landDark5 from "../../images/land-dark5.png";
import landDetail from "../../images/land-detail.png";
import landMessenger from "../../images/land-messenger.png";
import landCalendar from "../../images/land-calendar.png";
import tiktokVideo from "../../images/tiktok-video.mp4";

import Home from "../Home/Home";
import SingleIpad from "../SingleIpad/SingleIpad";
import DoubleIpad from "../DoubleIpad/DoubleIpad";
import DoubleIpadLand from "../DoubleIpadLand/DoubleIpadLand";
import FooterBottom from "../Footer/FooterBottom";
import ScrollAnimation from "../ScrollAnimation/ScrollAnimation";
import styles from "./Content.module.scss";
const Content = ({ loading }) => {
  return (
    <div className={loading ? "animation-hidden" : "animation-visible"}>
      <Home />
      <SingleIpad
        isVideo="false"
        title="Minimalism"
        description="Going minimal is the ultimate sophistication in websites. I am devoted to convey most information by using least contents for the users. My design embraces simplicity on every level."
        imageUrl={landMessenger}
      />
      <DoubleIpad
        title="Responsive"
        description="My work consists of a mix of flexible grids and layouts, images and an intelligent use of CSS media queries. As the user switches their devices, the website should automatically switch to accommodate for resolution, image size and scripting abilities."
        imageUrlLeft={landClothes}
        imageUrlRight={vertClothes}
      />
      <SingleIpad
        isVideo="false"
        smallTitle="Designed by Irina Krotkova"
        title="Aesthetic"
        description="Designing a beautiful web page can be tricky, but I'm here to help. I focus on high-quality images, modern typography and contrasting colors to create aesthetic user interfaces."
        imageUrl={landCalendar}
      />
      {/* <Tools /> */}
      <ScrollAnimation />

      <section className="phone-view section-padding">
        <SingleIpad
          isVideo="false"
          title="Animation"
          description="Animation is complicated. I'm constantly looking at ways to optimise user experiences with different animations on text and images. There are no best ways to use animations, but only beautiful ways."
          imageUrl={landSearch}
        />
      </section>
      <DoubleIpadLand
        title="Dark Mode"
        description="Dark theme is one of the most requested features over the past few years. Most companies made a dark theme an essential part of UI. Dark theme’s reduced luminance provides safety in dark environments and can minimize eye strain."
        imageUrlLeft={landDark3}
        imageUrlRight={landDark5}
      />
      <SingleIpad
        isVideo="false"
        title="User Experiences"
        description="I focus on anticipating what users might need to do and ensuring that the interface has elements that are easy to access, understand, and use to facilitate those actions. I am also devoted to improve the quality of the user’s interaction with and perceptions of your product and any related services."
        imageUrl={landDetail}
      />
      <SingleIpad
        isVideo="false"
        title="Color Theory"
        description="Color is not only an incredibly powerful part of one's website, it plays a crucial role in defining the user's experience. The art of color is supposed to create a digital experience the users will better engage with."
        imageUrl={abstractLand}
      />
      <SingleIpad
        smallTitle="Designed by Tiktok"
        isVideo="true"
        title="Contact"
        imageUrl={tiktokVideo}
      />
      <FooterBottom />
    </div>
  );
};

export default Content;
