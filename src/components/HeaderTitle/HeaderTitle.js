import styles from "./HeaderTitle.module.scss";

const HeaderTitle = ({ title, description }) => {
  return (
    <div className={["col-md-8", "block-center"].join(" ")}>
      <h2 className="title-headline">{title}</h2>
      <div className="content-headline subtext-color">{description}</div>
    </div>
  );
};

export default HeaderTitle;
