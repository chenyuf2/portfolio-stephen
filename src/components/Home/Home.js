import styles from "./Home.module.scss";

import wonderwoman from "../../images/wonder-woman.png";
import landEarth from "../../images/land-earth.png";
import landInterstellar from "../../images/land-interstellar.png";
import landLucid from "../../images/landLucid.png";
import landLotr3 from "../../images/lotr3.png";
import landShoe from "../../images/land-shoe.png";
const Home = () => {
  return (
    <div className={["mb-5 pb-5", styles["home-shader-container"]].join(" ")}>
      <div className="d-flex justify-content-center">
        <div className="grid-container home-animation-container">
          {/* //ipad 1 */}
          <div className={styles["grid-top-left"]}>
            <div className="ipad w-100 m-0 opacity-0">
              <img src={landLotr3} alt="img"></img>
            </div>
          </div>
          <div
            className={[
              styles["grid-top"],
              styles["grid-offset-vertical"],
            ].join(" ")}
          >
            <div className="ipad m-0 w-100 opacity-0">
              <img src={landLucid} alt="img"></img>
            </div>
          </div>
          <div className={styles["grid-top-right"]}>
            <div className="ipad m-0 w-100 opacity-0">
              <img src={wonderwoman} alt="img"></img>
            </div>
          </div>
          <div className={styles["grid-bottom-left"]}>
            <div className="ipad m-0 w-100 opacity-0">
              <img src={landEarth} alt="img"></img>
            </div>
          </div>
          <div
            className={[
              styles["grid-bottom"],
              styles["grid-offset-vertical"],
            ].join(" ")}
          >
            <div className="ipad m-0 w-100 opacity-0">
              <img src={landShoe} alt="img"></img>
            </div>
          </div>
          <div className={styles["grid-bottom-right"]}>
            <div className="ipad m-0 w-100 opacity-0">
              <img src={landInterstellar} alt="img"></img>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-3 container mb-5 pb-5 portfolio-title-container text-center">
        <div className={["h2 text-center font-weight-bold mb-0"].join(" ")}>
          {/* <div className="mb-1 tool-title font-weight-bold ">
            Hi! I'm Chenyu.
          </div> */}
          <div className={["text-orange h5 mb-0 font-weight-bold"].join(" ")}>
            PORTFOLIO
          </div>
        </div>

        <div className={["home-title opacity-0"].join(" ")}>
          <div>Everything you can</div>
          <div>imagine is real.</div>
        </div>
        <div className="col-md-8 opacity-0 block-center">
          <h4 className="home-subtitle text-center subtitle-text">
            Hi! I'm Stephen, a
            <span className="text-white"> front end developer</span> of
            ByteDance based in Mountain View. I am keen on designing interfaces,
            animations, and websites and everything in-between.
          </h4>
        </div>
      </div>
    </div>
  );
};

export default Home;
