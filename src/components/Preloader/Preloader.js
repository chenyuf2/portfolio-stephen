import styles from "./Preloader.module.scss";
import CountUp from "react-countup";
import gsap, { Expo } from "gsap";
import { useEffect } from "react";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { fadeInTitleDelay } from "../../utils/utilVar";

const Preloader = ({ loading, setLoading }) => {
  gsap.registerPlugin(ScrollTrigger);
  useEffect(() => {
    gsap.from(".preload-animate span", {
      y: 20,
      opacity: 0,
    });
  });
  const fadeInDurationTime = 1.3;
  const fadeInDelay = 1.3;
  const easeFn = Expo.easeInOut;
  const loadingAnimation = () => {
    const timeLine = gsap.timeline();
    timeLine
      .to(".preload-animate span", {
        y: -20,
        opacity: 0,
        onComplete: () => {
          setLoading(false);
        },
      })
      .fromTo(
        ".home-animation-container > div:nth-child(5) > div",
        {
          y: 20,
          autoAlpha: 0,
        },
        {
          y: 0,
          autoAlpha: 1,
          delay: 0.3,
        }
      )
      .fromTo(
        ".home-animation-container > div:nth-child(2) > div",
        {
          y: -50,
          autoAlpha: 0,
        },
        {
          y: 0,
          autoAlpha: 1,
          duration: fadeInDurationTime,
          ease: easeFn,
        },
        fadeInDelay
      )
      .fromTo(
        ".home-animation-container > div:nth-child(1) > div",
        {
          x: -50,
          autoAlpha: 0,
        },
        {
          x: 0,
          autoAlpha: 1,
          duration: fadeInDurationTime,
          ease: easeFn,
        },
        fadeInDelay
      )
      .fromTo(
        ".home-animation-container > div:nth-child(4) > div",
        {
          x: -50,
          y: 25,
          autoAlpha: 0,
        },
        {
          x: 0,
          y: 0,
          autoAlpha: 1,
          duration: fadeInDurationTime,
          ease: easeFn,
        },
        fadeInDelay
      )
      .fromTo(
        ".home-animation-container > div:nth-child(3) > div",
        {
          x: 50,
          autoAlpha: 0,
        },
        {
          x: 0,
          autoAlpha: 1,
          duration: fadeInDurationTime,
          ease: easeFn,
        },
        fadeInDelay
      )
      .fromTo(
        ".home-animation-container > div:nth-child(6) > div",
        {
          x: 50,
          y: 25,
          autoAlpha: 0,
        },
        {
          x: 0,
          y: 0,
          autoAlpha: 1,
          duration: fadeInDurationTime,
          ease: easeFn,
        },
        fadeInDelay
      )
      .fromTo(
        ".portfolio-title-container > div",
        {
          visibility: "hidden",
          y: 40,
        },
        {
          visibility: "visible",
          y: 0,
          stagger: 0.1,
          delay: fadeInTitleDelay,
        }
      );
  };
  return (
    <div
      className={[
        styles["preload-container"],
        "position-absolute preload-animate",
        loading ? "animation-visible" : "d-none",
      ].join(" ")}
    >
      <div
        className="h-100 w-100 d-flex align-items-center justify-content-center"
        id="countUp"
      >
        <CountUp
          className="Vultura-font display-3 m-0 font-weight-bold"
          start={0}
          end={100}
          useEasing={false}
          duration={7.25}
          onEnd={loadingAnimation}
          delay={0.3}
        />
      </div>
    </div>
  );
};
//
export default Preloader;

//
